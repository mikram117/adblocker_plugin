import os, sys, codecs, fnmatch
import urllib2
from bs4 import BeautifulSoup 
from urlparse import urljoin 
from urlparse import urlparse

fdisconnectpaths = [] 
# Listing rootdir, subdirectories (dirnames), and files in both rootdir & dirnames
fdisconnectfiles = [] # will consist of disconnect filenames
#jspath = '/home/ikram/Work/upptools/repos/areweprivateyet/doms/disconnectJS/'
jspath = '/Users/ikram/Desktop/GroundTruth-New/Data/WDS/Test/JSs/GtJS/' # absolute path for JSes to be downloaded
for root, dirnames, filenames, in os.walk('/Users/ikram/Desktop/GroundTruth-New/Data/WDS/Test/DOMS/GtTest/'): # absolute path for DOM tree
        for filename in fnmatch.filter(filenames, "*-ghostery"):
            fdisconnectfiles.append(filename)
            fdisconnectpaths.append(os.path.join(root, filename))
            os.system("mkdir %s%s"%(jspath, filename))
count = 0

for fname in fdisconnectfiles:
    fdopen = unicode(codecs.open("%s%s"%('/Users/ikram/Desktop/GroundTruth-New/Data/WDS/Test/DOMS/GtTest/', fname), "rb").read(), errors=ignore)
    soup = BeautifulSoup(fdopen) 
    sources = [] # For external JavaScripts
    scripts = [] # For in-page JavaScripts
  
    for script in soup(('script', {'type': 'text/javascript'})):
        src = script.get('src')
        if src:
            sources.append(src)   
        else: 
            if script.text in surrogate.keys():
                continue
            scripts.append(script.text)
    
    for i in range(len(scripts)):
        f = codecs.open("%s%s/%s%s.js" % (jspath,fname,fname, str(i+1)),"w", "utf-8")
        f.write(scripts[i])
        f.close()

    for source in sources:
        if (urlparse(source).netloc == ''):
            if source[0] == ".":
                url = "http://www." + fname.split("-ghostery")[0] + source[1:]
                os.system("wget --waitretry=1 --read-timeout=20 --timeout=15 -t 0 -nv -p -nd -R '*.gif *.html *.htm *.jpg *.jpeg *.css *.swf *.php' -A '*.js' -P %s%s/ -U mozilla %s " % (jspath, fname, url))
                continue
            elif source[0] == '/' and urlparse(source).netloc == '':
                url = "http://www." + fname.split("-ghostery")[0] + source
                os.system("wget --waitretry=1 --read-timeout=20 --timeout=15 -t 0 -nv -p -nd -R '*.gif *.html *.htm *.jpg *.jpeg *.css *.swf *.php' -A '*.js' -P %s%s/ -U mozilla %s " % (jspath, fname, url))
                continue
            else:
                os.system("wget --waitretry=1 --read-timeout=20 --timeout=15 -t 0 -nv -p -nd -R '*.gif *.html *.htm *.jpg *.jpeg *.css *.swf *.php' -A '*.js' -P %s%s/ -U mozilla %s " % (jspath, fname, urljoin("http://", source)))                
    count = count + 1 
    print(count)
