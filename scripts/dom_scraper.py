from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import time, random, codecs
import pickle

# Domain names to be scrapped

'''
fn = open('/Users/ikram/Desktop/GroundTruth-New/RefinedGT/DOMsTobeScrapped/AdblockPlusdomstbs.txt', 'r')
urls = []
for url in fn:
    urls.append(url.rstrip())
fn.close()
'''
urls = ['bbc.com',        
        'nytimes.com',
        'cnn.com',
        'cricinfo.com',
        'facebook.com',
        'google.com',
        'twitter.com',
        'youtube.com',
        'wordpress.com',
        'wsj.com']


# Profile settings in Ubuntu
#fp = webdriver.FirefoxProfile("/home/ikram/.mozilla/firefox/baseline/")
#fp.add_extension("/home/ikram/.mozilla/firefox/ghostery/extensions/firefox@ghostery.com.xpi")
#fn = open('/home/ikram/repo/DOMS/top5000.list-intrupted', 'r')


# Profile settings in Mac OS
fp = webdriver.FirefoxProfile("/Users/ikram/Library/Application Support/Firefox/Profiles/adblockplus/")
fp.add_extension("/Users/ikram/Library/Application Support/Firefox/Profiles/adblockplus/extensions/{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}.xpi")

driver = webdriver.Firefox(firefox_profile=fp)
count = 0
for url in urls1:
    count = count + 1
    start = time.time()
    try:
        driver.get("http://www." + url)
        driver.implicitly_wait(30)
        driver.set_page_load_timeout(600)   
        time.sleep(6+ abs(random.normalvariate(1.8,3)))
    except TimeoutException:
        driver.quit()
        time.sleep(3+ abs(random.normalvariate(1.8,3)))
        driver.get("http://www." + url)
        driver.implicitly_wait(60)
        driver.set_page_load_timeout(1200)   
        time.sleep(6+ abs(random.normalvariate(1.8,3)))    
    html = driver.execute_script("return document.documentElement.outerHTML")
    fnson = open("/Users/ikram/Desktop/GroundTruth-New/doms/abp-easylist/%s-abp-easylist"% url, "w")
    pickle.dump(html, fnson)
    print("%s :: %s :: %s" % (count, url, (time.time()-start)))    