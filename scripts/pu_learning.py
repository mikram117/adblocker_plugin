sys.path.insert(1, '/Users/ikram/Repos/Zamar/upptools/repos/pyscripts/pu-learning/')
sys.path.insert(0, '/Users/ikram/Repos/Zamar/upptools/repos/pyscripts/pu-learning/src/puLearning/')

import numpy as np
from sklearn.svm import SVC
from sklearn.datasets import make_classification
from __future__ import division
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import metrics
from operator import itemgetter
from sklearn.metrics import classification_report
import csv, os, codecs, fnmatch
import numpy as np
from sklearn import metrics
from sklearn import svm
from sklearn import cross_validation
import pandas as pd

gmv = 0.005 
class PUAdapter(object):

    def __init__(self, estimator, hold_out_ratio=0.1, precomputed_kernel=False):
        self.estimator = estimator
        self.c = 1.0
        self.hold_out_ratio = hold_out_ratio
        self.fit = self.__fit_no_precomputed_kernel
    def __fit_no_precomputed_kernel(self, X, y):
        positives = np.squeeze(np.asarray(np.where(y == 1.) [0]))
        #print("shape of positives  ")# % positives.shape)
        hold_out_size = np.ceil(len(positives) * self.hold_out_ratio)
        print ("Length of Positives: %s" % len(positives))#.shape[1])
        #print ("Hold out size %s" % hold_out_size)
        if len(positives) <= hold_out_size:
            raise Exception('Not enough positive examples to estimate p(s=1|y=1,x). Need at least ' + 
                            str(hold_out_size + 1) + '.')
        
        np.random.shuffle(positives)
        hold_out = positives[:hold_out_size]
        print ("hold_out_size %s " % (hold_out_size))
        #print (len(hold_out))
        X_hold_out = X[hold_out]
        X = np.delete(X, hold_out,0)
        y = np.delete(y, hold_out)
        print 'amount of new labels: ' + str(y.shape)
        print (X.shape)
        self.estimator.fit(X, y)
        hold_out_predictions = self.estimator.predict_proba(X_hold_out)
        try:
            hold_out_predictions = hold_out_predictions[:,1]
        except:
            pass
        c = np.mean(hold_out_predictions)
        self.c = c
        self.estimator_fitted = True
    def predict_proba(self, X):
        if not self.estimator_fitted:
            raise Exception('The estimator must be fitted before calling predict_proba(...).')
        probabilistic_predictions = self.estimator.predict_proba(X)
        #print ("The shape of new X is : ")#, X.shape)
        try:
            probabilistic_predictions = probabilistic_predictions[:,1]
        except:
            pass
        return probabilistic_predictions / self.c
    def predict(self, X, treshold=0.5):
        if not self.estimator_fitted:
            raise Exception('The estimator must be fitted before calling predict(...).')
        return np.array([1. if p > treshold else -1. for p in self.predict_proba(X)])

####################### Features Extraction
usefulJSpaths = [] # absolute path of Javascript path
usefulJSfiles = [] # list of file names
rusefulJSfiles = [] # list of opened files

for root, dirnames, filenames, in os.walk('/Users/ikram/Desktop/GroundTruth-New/Data/GTPDG/Useful/'):
    for filename in fnmatch.filter(filenames, "*.js"):
        usefulJSfiles.append(filename)
        usefulJSpaths.append(os.path.join(root, filename))
        try:
            with codecs.open(os.path.join(root, filename)) as f: # No need to specify 'r': this is the default.
                rusefulJSfiles.append(unicode(f.read(), errors='ignore')) # inline Cosine similarity, Cosine does work on whole doc so no .split() 
        except IOError as exc:
            if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
                raise # Propagate other kinds of IOError.

uselessJSpaths = [] # absolute path of Javascript path
uselessJSfiles = [] 
ruselessJSfiles = [] # list of opened files


for ulessroot, ulessdirnames, ulessfilenames, in os.walk('/Users/ikram/Desktop/GroundTruth-New/Data/GTPDG/useless/'):
    for filename in fnmatch.filter(ulessfilenames, "*.js"):
        uselessJSfiles.append(filename)
        uselessJSpaths.append(os.path.join(ulessroot, filename))
        try:
            with codecs.open(os.path.join(ulessroot, filename)) as f: # No need to specify 'r': this is the default.
                ruselessJSfiles.append(unicode(f.read(), errors='ignore')) # inline Cosine similarity, Cosine does work on whole doc so no .split() 
        except IOError as exc:
            if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
                raise # Propagate other kinds of IOError.


                
print ("Number of useless JS files: %s " % len(ruselessJSfiles))
print ("Number of useful JS files: %s " % len(rusefulJSfiles))
print ("useful JS vs useless JS (ratio) : %s " % (len(rusefulJSfiles)/len(ruselessJSfiles)))
################### Feature space formation
tfidf_vectorizer = TfidfVectorizer(max_df=0.5, max_features=200,
                                 min_df=2)

tfidf_matrix_uselessJS = tfidf_vectorizer.fit_transform(ruselessJSfiles)  #finds the tfidf score with normalization
tfidf_matrix_usefulJS = tfidf_vectorizer.fit_transform(rusefulJSfiles)  #finds the tfidf score with normalization

fcreport = [] # fincal classification report
ufratio = [round((x+1)/10* len(rusefulJSfiles)) for x in range(9)]
testr = [len(rusefulJSfiles) - x for x in ufratio]

#pscore = [] # pricision score
#fcml = [] # final confusion matrix
#rcall = [] # recall
#accuracy = [] # accuracy of classifier
#f1score = [] # f1 score 
#tp = [] # True positives
#tn = [] # True negatives
#fp = [] # False positives
#fn = [] # False negative

for xratio in ufratio:
    # Training set
    trainuseful = np.c_[tfidf_matrix_usefulJS[:xratio, :].todense(), np.zeros(tfidf_matrix_usefulJS[:xratio, :].todense().shape[0])]
    trainuseless = np.c_[tfidf_matrix_uselessJS[:xratio,:].todense(), np.ones(tfidf_matrix_uselessJS[:xratio,:].todense().shape[0])]

    # Testing set 
    testuseful = np.c_[tfidf_matrix_usefulJS[xratio:, :].todense(),  np.zeros(tfidf_matrix_usefulJS[xratio:, :].todense().shape[0])]  
    testuseless = np.c_[tfidf_matrix_uselessJS[xratio:,:].todense(), np.ones(tfidf_matrix_uselessJS[xratio:,:].todense().shape[0])]  
    
    #ftestuseful[:,-1] # accessing the label column of useful javascripts tests
    #print ("Results for %s percentage of traning data" % xratio)
    #print (trainuseful.shape)
    #print (trainuseless.shape)
    #print (testuseful.shape)
    #print (testuseless.shape)
    
    X_test = np.r_[testuseful, testuseless]
    X_train = np.r_[trainuseful, trainuseless]
    
    X = np.r_[X_test, X_train]
    y = X[:,-1] # separating labels
    ######################## Applying Classifiers
    y[np.where(y == 0)] = -1. # Unlabelled samples are labelled as -1

    X1 = np.squeeze(np.asarray(X[:,0:199])) # matrix to array conversion
    y1 = np.squeeze(np.asarray(y))

    # SVM is used as benchmark for comparisons. 
    clf = svm.SVC(C=10000.0, cache_size=200, class_weight=None, coef0=0.0, degree=3,
      gamma=gmv, kernel='rbf', max_iter=-1, probability=True,
      random_state=None, shrinking=True, tol=0.1, verbose=False)
    
    pu_estimator = PUAdapter(clf, hold_out_ratio=0.5)
    
    ####################### Prediction Phase
    pu_estimator.fit(X1, y1)
    ####################### Prediction Phase
    
    print ("Comparison of SSVM and PUAdapter(estimator):")

    print 'Number of disagreements: ' + str(len(np.where((pu_estimator.predict(X1) == clf.predict(X1)) == False)[0]))
    print 'Number of agreements: ' + str(len(np.where((pu_estimator.predict(X1) == clf.predict(X1)) == True)[0]))
    
    print 'The precision for SVM classifier is ' + str(metrics.precision_score(y1, clf.predict(X1)))
    print 'The recall for SVM classifier is ' + str(metrics.recall_score(y1, clf.predict(X1)))
    print 'The f1 for SVM classifier is ' + str(metrics.f1_score(y1, clf.predict(X1)))
    print 'The accuracy for SVM classifier is ' + str(metrics.accuracy_score(y1, clf.predict(X1)))
    print '\nHere is SVM classification report of SVM:'
    print classification_report(y1, clf.predict(X1))
    print '\nHere is the confusion matrix:'
    print metrics.confusion_matrix(y1, clf.predict(X1))#, labels=unique(X_train[:,-1]))
    
    

    print 'The precision for PU Learning based classifier is ' + str(metrics.precision_score(y1, pu_estimator.predict(X1)))
    print 'The recall for PU Learning based classifier is ' + str(metrics.recall_score(y1, pu_estimator.predict(X1)))
    print 'The f1 for PU Learning based classifier is ' + str(metrics.f1_score(y1, pu_estimator.predict(X1)))
    print 'The accuracy for PU Learning based classifier is ' + str(metrics.accuracy_score(y1, pu_estimator.predict(X1)))
    print '\nHere is PU Learning based classification report:'
    print classification_report(y1, pu_estimator.predict(X1))
    print '\nHere is the confusion matrix:'
    print metrics.confusion_matrix(y1, pu_estimator.predict(X1))
    
    