from __future__ import division
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import metrics
from operator import itemgetter
from sklearn.metrics import classification_report
import csv, os, codecs, fnmatch
import numpy as np
from sklearn import metrics
from sklearn import svm
from sklearn import cross_validation
import pandas as pd

usefulJSpaths = [] # absolute path of Javascript path
usefulJSfiles = [] # list of file names
rusefulJSfiles = [] # list of opened files
n_features = 190

gmv = 0.001 # 0.001 # 0.05 # 0.1 # 0.5 # 0.1, 0.05, 0.01, 0.005, 0.001
nuv = 0.001 # 0.1, 0.05, 0.01, 0.005, 0.001

for root, dirnames, filenames, in os.walk('/Users/ikram/Desktop/GroundTruth-New/Data/GTPDG/Useful/'):
    for filename in fnmatch.filter(filenames, "*.js"):
        usefulJSfiles.append(filename)
        usefulJSpaths.append(os.path.join(root, filename))
        try:
            with codecs.open(os.path.join(root, filename)) as f: # No need to specify 'r': this is the default.
                rusefulJSfiles.append(unicode(f.read(), errors='ignore')) # inline Cosine similarity, Cosine does work on whole doc so no .split() 
        except IOError as exc:
            if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
                raise # Propagate other kinds of IOError.

uselessJSpaths = [] # absolute path of Javascript path
uselessJSfiles = [] 
ruselessJSfiles = [] # list of opened files


for ulessroot, ulessdirnames, ulessfilenames, in os.walk('/Users/ikram/Desktop/GroundTruth-New/Data/GTPDG/useless/'):
    for filename in fnmatch.filter(ulessfilenames, "*.js"):
        uselessJSfiles.append(filename)
        uselessJSpaths.append(os.path.join(ulessroot, filename))
        try:
            with codecs.open(os.path.join(ulessroot, filename)) as f: # No need to specify 'r': this is the default.
                ruselessJSfiles.append(unicode(f.read(), errors='ignore')) # inline Cosine similarity, Cosine does work on whole doc so no .split() 
        except IOError as exc:
            if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
                raise # Propagate other kinds of IOError.


                
print ("Number of useless JS files: %s " % len(ruselessJSfiles))
print ("Number of useful JS files: %s " % len(rusefulJSfiles))
print ("useful JS vs useless JS (ratio) : %s " % (len(rusefulJSfiles)/len(ruselessJSfiles)))
################### Feature space formation
tfidf_vectorizer = TfidfVectorizer(max_df=0.5, max_features=190,
                                 min_df=2)

tfidf_matrix_uselessJS = tfidf_vectorizer.fit_transform(ruselessJSfiles)  #finds the tfidf score with normalization
tfidf_matrix_usefulJS = tfidf_vectorizer.fit_transform(rusefulJSfiles)  #finds the tfidf score with normalization

################## Training Phase

fcreport = [] # fincal classification report
ufratio = [round((x+1)/10* len(rusefulJSfiles)) for x in range(9)]
testr = [len(rusefulJSfiles) - x for x in ufratio]

pscore = [] # pricision score
fcml = [] # final confusion matrix
rcall = [] # recall
accuracy = [] # accuracy of classifier
f1score = [] # f1 score 
tp = [] # True positives
tn = [] # True negatives
fp = [] # False positives
fn = [] # False negative

for xratio in ufratio:
    trainuseful = np.c_[tfidf_matrix_usefulJS[:xratio, :].todense(), np.ones(tfidf_matrix_usefulJS[:xratio, :].todense().shape[0])]
    trainuseless = np.c_[tfidf_matrix_uselessJS[:xratio,:].todense(), -1 * np.ones(tfidf_matrix_uselessJS[:xratio,:].todense().shape[0])]

    # Testing set 
    testuseful = np.c_[tfidf_matrix_usefulJS[xratio:, :].todense(), np.ones(tfidf_matrix_usefulJS[xratio:, :].todense().shape[0])]  
    testuseless = np.c_[tfidf_matrix_uselessJS[xratio:,:].todense(), -1 * np.ones(tfidf_matrix_uselessJS[xratio:,:].todense().shape[0])]  
    
    print ("Results for %s percentage of traning data" % xratio)
    print (trainuseful.shape)
    print (trainuseless.shape)
    print (testuseful.shape)
    print (testuseless.shape)
    
    X_test = np.r_[testuseful, testuseless]
    X_train = np.r_[trainuseful, trainuseless]

  
    OneCX_test = testuseless[:, 0:189]
    OneCX_train = X_train[:, 0:189]
    OneCX_Labels = testuseless[:,-1]

    clf = svm.OneClassSVM(cache_size=200, coef0=0.0, degree=3, gamma=gmv, nu=nuv, kernel='rbf', max_iter=-1,
      random_state=None, shrinking=True, tol=0.1, verbose=False)
        
    clf.fit(OneCX_train)
    y_nb_predicted = clf.predict(OneCX_test)
        
    print ('The precision for this classifier is ' + str(metrics.precision_score(testuseless[:,-1], y_nb_predicted)))
    pscore.append(metrics.precision_score(testuseless[:,-1], y_nb_predicted))
    print ('The recall for this classifier is ' + str(metrics.recall_score(testuseless[:,-1], y_nb_predicted)))
    rcall.append(metrics.recall_score(testuseless[:,-1], y_nb_predicted))
    print ('The f1 for this classifier is ' + str(metrics.f1_score(testuseless[:,-1], y_nb_predicted)))
    f1score.append(metrics.f1_score(testuseless[:,-1], y_nb_predicted))
    print ('The accuracy for this classifier is ' + str(metrics.accuracy_score(testuseless[:,-1], y_nb_predicted)))
    accuracy.append(metrics.accuracy_score(testuseless[:,-1], y_nb_predicted))
    
    print ('\nHere is the classification report:')
    print (classification_report(testuseless[:,-1], y_nb_predicted))

    print ('\nHere is the confusion matrix:')
    print (metrics.confusion_matrix(testuseless[:,-1], y_nb_predicted))
    trpo = len(testuseful[:,-1]) + 1
    cm2 =  metrics.confusion_matrix(testuseful[:,-1], y_nb_predicted[1:trpo])
    print ("Confusion metrix for negative examples: \n")
    print cm2
    